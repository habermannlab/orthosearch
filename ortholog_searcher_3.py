#Orthologs searching for one protein

#Importing
import re, os, sys, subprocess, time, tempfile, shutil, struct, multiprocessing
import math
from Bio.Blast import NCBIXML
from optparse import OptionParser
from functions import Struct, u, error, parse_input_fasta,\
get_input_fasta_first_record
##

#Global variables
cheader = re.compile('^>')
cseq = re.compile('^[ACDEFGHIKLMNPQRSTVWYUXJBZ\*]+$')
cblank = re.compile('^[\ \t]*$')
cerror1 = re.compile('^Selenocysteine')
g_taxid_group_map = {'40674': 'mammals', '8782': 'birds', '8459': 'reptiles',\
'8504': 'reptiles', '1294634': 'reptiles', '7898': 'fishes', '6960':\
'insects', '6231': 'worms', '35493': 'plants', '3041': 'green_algae', '33682':\
'euglenozoa', '4751': 'fungi', '10239': 'viruses', '1': '', '32630':\
'synthetic'}
##
g_nr_use = ('green_algae', 'euglenozoa', 'fungi')

#Processing command line arguments
usage = '%prog [options] -i INPUT_FILE -o OUTPUT_FILE'
parser = OptionParser(usage = usage)
parser.add_option('-i', metavar = "INPUT_FILE", dest = 'input_file', help =\
'Specify file to read. INPUT_FILE must be conventional one-record fasta')
##
parser.add_option('-o', metavar = 'OUTPUT_FILE', dest = 'output_file',
                  help = 'Output FASTA with orthologs')
parser.add_option('-a', '--auto', dest = 'auto_db', action = 'store_true',
                  default = False, help = 'Auto db and gi list')
parser.add_option('-d', '--database', dest = 'database',
                  default = 'refseq_protein', help = 'BLAST database')
parser.add_option('-t', dest = 'taxonomy_dir', default = '../taxonomy/',
                  help = 'Directory with taxonomy-related files')
parser.add_option('-g', dest = 'gilist', default = '', help = 'gilist file')
parser.add_option('-c', type = 'int', dest = 'cpus', default = 8,
                  help = 'Number of CPUs to use')
parser.add_option('-l', type = 'float', dest = 'coverage', default = 0,\
help = 'Mininmal coverage of input sequence by alignment, [0, 1]')
##
parser.add_option('-q', type = 'int', dest = 'id_min', default = 0,\
help = 'Mininmal % identity in the alignment, [0, 100]')
##
parser.add_option('-p', dest = 'precalc_db_dir', default = '',
                  help = 'Directory with precalculated orthologs database')
if __name__ == "__main__":
    if len(sys.argv) == 1:
       parser.print_help()
       sys.exit()
    (opt, args) = parser.parse_args()
    if not opt.input_file:
        error('Input file is not specified')
    opt.input_file = os.path.abspath(opt.input_file)
    if not os.path.isfile(opt.input_file):
        error('Input file does not exist')
    if not opt.output_file:
        sys.stderr.write("Error: Output file is not specified")
    opt.output_file = os.path.abspath(opt.output_file)
    if opt.gilist:
        if not os.path.isfile(opt.gilist):
            error('GI list file does not exist')
        opt.gilist = os.path.abspath(opt.gilist)
    opt.taxonomy_dir = os.path.abspath(opt.taxonomy_dir) + '/'
    if not os.path.isdir(opt.taxonomy_dir):
        error('Taxonomy directory does not exist')
    if opt.cpus < 1:
        error('Number of CPUs must be a positive integen')
    if (opt.coverage > 1.0) or (opt.coverage < 0.0):
        error('Minimal coverage must be in range [0, 1]')
    if (opt.id_min > 100) or (opt.id_min < 0):
        error('Minimal % identity must be in range [0, 100]')
    if opt.precalc_db_dir:
        if not os.path.isdir(opt.precalc_db_dir):
            error('Precalculated orthologs directory does not exist')
        opt.precalc_db_dir = os.path.abspath(opt.precalc_db_dir) + '/'

#Check for availability of executables
for binary in ['blastp', 'blastdbcmd']:
    found = None
    stdout, stderr = subprocess.Popen(['which', binary], stdout =\
    subprocess.PIPE, stderr = subprocess.PIPE).communicate()
    ##
    if not stdout:
        error('{} is not available\n'.format(binary))
        sys.exit(1)

def gi_to_species(gi, taxonomy_dir):
    """Function for determining soecies and species group based on GI"""
    
    #Determining taxonomy id
    gi = str(gi)
    taxid = 0
    gi_taxid_db = taxonomy_dir + 'gi_taxid_prot.dmp'
    with open(gi_taxid_db, 'r') as ifile:
        for line in ifile:
            fields = line.split('\t')
            if gi ==fields[0]:
                taxid = int(fields[1])
                break
    taxid0 = taxid
    
    #Determining custom taxonomy group
    dict_ = {}
    classification_db = taxonomy_dir + 'nodes_short.bin'
    with open(classification_db, 'rb') as ifile:
        bytes_read = ifile.read(8)
        while bytes_read:
            node, parent = struct.unpack('II', bytes_read)
            dict_[node] = parent
            bytes_read = ifile.read(8)
    while True:
        found = False
        for node in dict_:
            if node == taxid:
                taxid = dict_[node]
                found = True
                break
        if found:
            if str(taxid) in g_taxid_group_map:
                break
        else:
            taxid = '1'
            break
    
    group = g_taxid_group_map[str(taxid)]
    if group:
        gi_list_group = taxonomy_dir + 'gi_{}.txt'.format(group)
        db = 'nr' if group in g_nr_use else 'refseq_protein'
    else:
        gi_list_group = ''
        db = 'refseq_protein'
    gi_list_species = taxonomy_dir + 'gi_{}.txt'.format(taxid0)
    if not os.path.isfile(gi_list_species):
        gi_list_species = gi_list_group
    return db, gi_list_group, gi_list_species

#---Defining functions---
def trim_fasta_headers(input_file, max_len):
    """Function for trimming headers in a FASTA file"""
    tmp_interm = tempfile.NamedTemporaryFile('w+')
    with open(tmp_interm.name, 'w') as ofile:
        with open(input_file, 'r') as ifile:
            for line in ifile:
                if line.startswith('>') and len(line) > max_len:
                    line = line[0: max_len - 1] + '\n'
                ofile.write(line)
    shutil.copy(tmp_interm.name, input_file)
    tmp_interm.close()

def load_gilist(gilist):
    """Function for loading the GI list from file to a python set"""
    gilist_set = set()
    try:
        with open(gilist, 'r') as ifile:
            for line in ifile:
                gilist_set.add(line.strip())
    except FileNotFoundError:
        pass
    return gilist_set

def blast_and_parse(input_file, database = 'refseq_protein', evalue = '0.1',
                    num_alignments = '2000', coverage = 0, id_min = 0,
                    gilist = '', cpus = 8, taxonomy_dir = '',
                    capture_alt_self_gis = False):
    """Function for running BLAST and parsing its output, saving only the \
best hit for each species"""
    ##
    blast_hits = [[] for i in range(2)]
    gilist_species = ''
    self_gis = []
    self_headers = []
    tmp_blast = tempfile.NamedTemporaryFile('w+')
    with open(os.devnull, 'wb') as devnull:
        check = subprocess.Popen(['blastp', '-db', database, '-query',\
        input_file, '-evalue', evalue, '-num_alignments', num_alignments,\
        '-outfmt', '5', '-out', tmp_blast.name, '-num_threads', str(cpus)] +\
        ['-gilist', gilist] * bool(gilist), stdout = devnull, stderr =\
        subprocess.PIPE)
        ##
        stdout, stderr = check.communicate()
        if stderr and (not cerror1.search(stderr.decode('utf-8'))):
            raise RuntimeError('Error in blastp: ' + u(stderr))
    result = NCBIXML.read(open(tmp_blast.name))
    tmp_blast.close()
    alignments = len(result.alignments)
    len_ = result.query_length
    anchor = False
    if capture_alt_self_gis:
        self_gis_candidates = []
    for i in range(alignments):
        if (result.alignments[i].length < len_ * coverage):
            continue
        ident = result.alignments[i].hsps[0].identities
        headers = result.alignments[i].hit_def.split('>')
        headers[0] = result.alignments[i].hit_id + ' ' + headers[0]
        for header in headers:
            if 'partial' in header:
                continue
            try:
                gi = header.split("|")[1]
                species = header.split('[')[1].split(']')[0]
                if ' x ' in species:
                    continue
                species = '{}_{}'.format(species.split()[0],
                                         species.split()[1])
            except IndexError:
                continue
            if capture_alt_self_gis and (not anchor):
                if ident != len_:
                    continue
                elif result.alignments[i].length > len_:
                    self_gis_candidates.append(Struct(gi = gi, species =\
                    species, len = result.alignments[i].length, header =\
                    header))
                    ##
                    continue
                anchor = True
                isoform = ('isoform ' in header)
                for candidate in self_gis_candidates:
                    if candidate.species == species:
                        if candidate.len <= len_ + 2:
                            self_gis.append(candidate.gi)
                            if 'isoform ' in candidate.header:
                                self_headers.append(candidate.header)
                        elif isoform and (header.split('isoform ')[0] ==\
                        candidate.header.split('isoform ')[0]):
                        ##
                            self_gis.append(candidate.gi)
                            self_headers.append(candidate.header)
                del self_gis_candidates
                self_gis.append(gi)
                if isoform:
                    self_headers.append(header)
                if taxonomy_dir:
                    database, gilist, gilist_species = gi_to_species(gi,\
                    taxonomy_dir)
                    ##
                    #gilist_set = load_gilist(gilist)
            if capture_alt_self_gis:
                #if gi not in gilist_set:
                    #continue
                if (ident / len_ < id_min * 0.01):
                    continue
                if len(blast_hits[0]) and (ident / len_ > 0.95):
                    continue
            if species not in blast_hits[1]:
                blast_hits[0].append(gi)
                blast_hits[1].append(species)
            elif capture_alt_self_gis and (species == blast_hits[1][0]):
                if (ident == len_) and (result.alignments[i].length <= len_ +\
                2):
                ##
                    self_gis.append(gi)
                elif 'isoform ' in header:
                    for header_ in self_headers:
                        if header_.split('isoform ')[0] ==\
                        header.split('isoform ')[0]:
                        ##
                            self_gis.append(gi)
                            break
    if not anchor:
        return blast_hits, database, '', '', []
    return blast_hits, database, gilist, gilist_species, self_gis

def back_blast_worker(gi, species, query_species, self_gis, database, gilist,
                      cpus):
    """Function for back-BLASTing a single hit in a pool of processes"""
    tmp_query = tempfile.NamedTemporaryFile('w+')
    with open(os.devnull, 'wb') as devnull:
        check = subprocess.Popen(['blastdbcmd', '-db', database,\
        '-dbtype', 'prot', '-entry', gi, '-out', tmp_query.name], stdout =\
        devnull, stderr = subprocess.PIPE)
        ##
        stdout, stderr = check.communicate()
        if stderr:
            if 'Entry not found' in u(stderr):
                tmp_query.close()
                return None
            return u(stderr)
    trim_fasta_headers(tmp_query.name, 1000)
    try:
        subhits = blast_and_parse(tmp_query.name, database = database,\
        gilist = gilist, cpus = cpus)[0]
        ##
    except Exception as e:
        return(str(e))
    found = False
    for j in range(len(subhits[0])):
        if subhits[1][j] == query_species:
            found = True
            if subhits[0][j] in self_gis:
                records = parse_input_fasta(tmp_query.name)
                seq = records[0][1]
                tmp_query.close()
                return gi, species, seq
            else:
                tmp_query.close()
                return None
    if not found:
        tmp_query.close()
        return None

def back_blast(blast_hits, self_gis, database = 'refseq_protein', gilist = '',
               cpus = 8):
    """Function for performing back-BLAST"""
    def append_result(result):
        """Closure to process individual back-BLASTing results"""
        if type(result) == str:
            error(result)
        if result:
            bblast_hits[0].append(result[0])
            bblast_hits[1].append(result[1])
            bblast_hits[2].append(result[2])
    bblast_hits = [[] for i in range(3)]
    len_ = len(blast_hits[0])
    cpus = math.ceil(cpus / max(len_, 1))
    pool = multiprocessing.Pool()
    for i in range(len_):
        pool.apply_async(back_blast_worker, args = (blast_hits[0][i],\
        blast_hits[1][i], blast_hits[1][0], self_gis, database, gilist, cpus),\
        callback = append_result)
        ##
    pool.close()
    pool.join()
    return bblast_hits

def fetch_db_orthologs(input_file, output_file, precalc_db_dir):
    """Function for fetching the orthologs from the database"""
    seq = get_input_fasta_first_record(input_file)[1]
    precalc_fpath = ''
    print('~1~{}~'.format(seq))##/
    with open(precalc_db_dir + 'index.tsv', 'r') as ifile:
        for line in ifile:
            if line.split('\t')[0] == seq:
                print('~2~{}~'.format(line))##/
                precalc_fpath = precalc_db_dir
                precalc_fpath += line.split('\t')[1].strip() + '.dat'
                break
    print('~3~{}~'.format(precalc_fpath))##/
    if precalc_fpath:
        print('#4')##/
        shutil.copy(precalc_fpath, output_file)
        print('#5')##/

#input BBhits
#get sequences and write fasta file---Orthologs of query seq
def write_output(bblast_hits, output_file, query_seq):
    """Function for writing the results to file"""
    with open(output_file, 'w') as file:
        if (len(bblast_hits[0]) == 0) or (bblast_hits[2][0] != query_seq):
            file.write('>0\n{}\n'.format(query_seq))
        for i in range(len(bblast_hits[0])):
            file.write('>{}_{} gi|{}\n{}\n'.format(i + 1,\
            bblast_hits[1][i].replace(' ', '_'), bblast_hits[0][i],\
            bblast_hits[2][i]))
            ##

#---Main section---
starttime = time.time()
print("Processing {}".format(opt.input_file.rsplit('/', 1)[1]))
print('Checking input... ', end = '')
sys.stdout.flush()
records = parse_input_fasta(opt.input_file)
if len(records) != 1:
    error('Input FASTA must contain exactly 1 sequence')
print('Done.')
print('Blasting... ', end = '')
sys.stdout.flush()
taxonomy_dir = opt.taxonomy_dir if opt.auto_db else ''
database = 'nr' if opt.auto_db else opt.database
gilist = '' if opt.auto_db else opt.gilist
blast_hits, database, gilist, gilist_species, self_gis = blast_and_parse(\
opt.input_file, database = database, coverage = opt.coverage, id_min =\
opt.id_min, gilist = gilist, cpus = opt.cpus, taxonomy_dir = taxonomy_dir,\
capture_alt_self_gis = True)
##
print('Done. Got best hits for {} species'.format(len(blast_hits[0])))
bblast_hits = back_blast(blast_hits, self_gis, database = database,
                         gilist = gilist_species, cpus = opt.cpus)
print('Done with back-blasting. There are {} supposed orthologs'.\
format(len(bblast_hits[0])))
##
print('Saving results into file...')

write_output(bblast_hits, opt.output_file, records[0][1])
print('Done with searching for orthologs of {}. Normal termination'.\
format(blast_hits[0][0] if blast_hits[0] else 'unknown sequence'))
##
if opt.precalc_db_dir:
    print('#)')##/
    fetch_db_orthologs(opt.input_file, opt.output_file, opt.precalc_db_dir)
    print('#9')##/
print('Runtime: {} sec'.format(int(time.time() - starttime)))
